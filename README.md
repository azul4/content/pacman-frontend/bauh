# bauh

Graphical interface for managing your applications (AppImage, Flatpak, Snap, Arch/AUR, Web)

https://github.com/vinifmor/bauh

<br><br>
How to clone this repository:

```
git clone https://gitlab.com/azul4/content/pacman-frontend/bauh.git
```
<br><br>
## Autostart: tray mode

In order to initialize bauh with the system, use your Desktop Environment settings to register it as a startup application / script (**bauh-tray**). Or create a file named **bauh.desktop** in **~/.config/autostart** . See the process below:

**1.** Create the file:

```
nano ~/.config/autostart/bauh.desktop
```


**2.** Inside copy:

```
[Desktop Entry]
Type=Application
Name=bauh (tray)
Exec=/usr/bin/bauh-tray
```

**3.** Save and exit:

**Ctrl+o** to save, and **Ctrl+x** to exit.

**4.** Restart the system.
